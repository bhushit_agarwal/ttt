package bagarwal.spreetail.ttt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author bhush
 */
public class Game {
    
    private final Integer playerCount;
    private final Integer boardSize;
    private List<Move> moves;
    private char[][] board;
    private List<Integer> availableMoves;
    
    public Game() {
        playerCount = 2;
        boardSize = 3;
        availableMoves = new ArrayList<>();
        moves = initPlayers(playerCount);
        board = initBoard(boardSize);
    }
    
    public Game(int boardSize, int players) {
        this.boardSize = boardSize;
        this.playerCount = players;
        availableMoves = new ArrayList<>();
        this.moves = initPlayers(players);
        this.board = initBoard(boardSize);
    }

    public Integer getPlayerCount() {
        return playerCount;
    }

    public Integer getBoardSize() {
        return boardSize;
    }

    public List<Move> getMoves() {
        return moves;
    }

    public char[][] getBoard() {
        return board;
    }

    public List<Integer> getAvailableMoves() {
        return availableMoves;
    }  
    
    private  char[][] initBoard(Integer boardSize) {
        char[][] board = new char[boardSize][boardSize];
        for(int i=0; i < boardSize; i++) {
            for(int j=0; j<boardSize; j++) {
                board[i][j] = ' ';
                availableMoves.add(i*boardSize + j);
            }   
        }
        return board;
    }
    
    private List<Move> initPlayers(int players) {
        List<Move> playersList = new ArrayList<>();
        for(int i=0; i< players; i++) {
            playersList.add(new Move());
        }
        return playersList;
    }
    
    /**
     * Add the played moved to player's move and evaluate the board for win. 
     * Returns true if played move is the winning move.
     * @param player
     * @param move
     * @return 
     */
    public boolean makeMove(Player player, int move) {
        int row = move/boardSize;
        int col = move%boardSize;
        char sym = player.getMarker();
        
        // Update the move on the board and in list of available moves
        board[row][col] = sym;
        for(int i=0; i < availableMoves.size(); i++) {
            if(availableMoves.get(i) == move) {
                availableMoves.remove(i);
                break;
            }
        }
        
        /* 
            Dividing position by boardSize gives following arrangement
            0 | 0 | 0
            ---------
            1 | 1 | 1
            ---------
            2 | 2 | 2
            It can be clearly observed that quotients in each row will have same value.
            This reduces time complexity to evaluate the board.
        */ 
        Map<Integer, List<Integer>> rows = moves.get(player.getId()).getRows();
        if(rows.containsKey(row)) {
            rows.get(row).add(move);
            if(rows.get(row).size() == boardSize)
                return true;
        } else {
            List<Integer> arrayList = new ArrayList<>();
            arrayList.add(move);
            rows.put(row, arrayList);
        }   
       
        /* 
            Modding position by boardSize gives following arrangement
            0 | 1 | 2
            ---------
            0 | 1 | 2
            ---------
            0 | 1 | 2
            It can be clearly observed that remainders in each column will have same value.
            This reduces time complexity to evaluate the board.
        */
        Map<Integer, List<Integer>> columns = moves.get(player.getId()).getColumns();
        if(columns.containsKey(col)) {
            columns.get(col).add(move);
            if(columns.get(col).size() == boardSize)
                return true;
        } else {
            List<Integer> arrayList = new ArrayList<>();
            arrayList.add(move);
            columns.put(col, arrayList);
        }   
        
        Map<Integer, List<Integer>> diagonals = moves.get(player.getId()).getDiagonals();
        
        /* 
           Quotient and remainder (row index and column index) are equal for left diagonal
            It can be used to check if the played move lies on the left diagonal
            This reduces time complexity to evaluate the board.
        */
        if(row == col) {
            if(diagonals.containsKey(0)) {
                diagonals.get(0).add(move);
                if(diagonals.get(0).size() == boardSize)
                    return true;
            }
            else {
                List<Integer> arrayList = new ArrayList<>();
                arrayList.add(move);
                diagonals.put(0, arrayList);
            } 
        }   
        
        /* 
           Sum of quotient and remainder (row index and column index) gives right diagonal
            It can be used to check if the played move lies on the right diagonal
            This reduces time complexity to evaluate the board.
        */
        if(row + col == boardSize-1) {
            if(diagonals.containsKey(1)) {
                diagonals.get(1).add(move);
                if(diagonals.get(1).size() == boardSize)
                    return true;
            } else {
                List<Integer> arrayList = new ArrayList<>();
                arrayList.add(move);
                diagonals.put(1, arrayList);
            }      
        } 
        
        return false;
    }

    /**
     * Check if the played position is already used. If used, it is an invalid move.
     * @param move
     * @return 
     */
    public boolean isValidMove(int move) {
        return board[move/(boardSize)][move%(boardSize)] == ' ';
    }
    
    public void printBoard() {
        Integer gridSize = boardSize * boardSize;
        Integer padding = gridSize.toString().length();
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<((padding+3)*boardSize)-2; i++) {
            sb.append('-');
        }
        for(int i=0; i< boardSize; i++) {
            for(int j=0; j < boardSize; j++) {
                if(board[i][j]==' ') {
                    System.out.printf("%"+padding+"d",(i*boardSize+j));
                }
                else {
                    for(int p=0; p<padding-1; p++) {
                        System.out.print(" ");
                    } 
                    System.out.print((char)board[i][j]);
                }
                if(j != boardSize -1) {
                    System.out.print(" | ");
                }
            }
            System.out.println("");
            if(i!=boardSize -1) {
                System.out.println(sb.toString());
            }
        }
        System.out.println("");
    }
    
    public boolean isStaleMate() {
        if(availableMoves.isEmpty()) {
            return true;
        }
        for(int i=0; i<boardSize; i++) {
            boolean rowsBlocked = true;
            boolean colsBlocked = true;

            for (Move move : moves) {                
                rowsBlocked = rowsBlocked && move.getRows().getOrDefault(i, new ArrayList<>()).size() > 0;
                colsBlocked = colsBlocked && move.getColumns().getOrDefault(i, new ArrayList<>()).size() > 0;
            } 
            if(!rowsBlocked || !colsBlocked) {
                return false;
            }
        }
        return true;
    }
}

