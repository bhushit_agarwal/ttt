package bagarwal.spreetail.ttt;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author bhush
 */
public class TTT {
    private static List<Character> markers = new ArrayList<>();
    private static Game g;
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in); 
        System.out.println("Welcome to TTT. A simple but mighty Tic Tac Toe game");
        System.out.println("Please enter number of players excluding the computer:");
        String playerCountStr = reader.nextLine();
        while(!isValidInteger(playerCountStr)) {
            playerCountStr = reader.nextLine();
        }
        int playerCount = Integer.parseInt(playerCountStr)+1;
        
        // Initialize players
        List<Player> players = new ArrayList<>();
        
        System.out.println("Please enter the board size. It must be an odd number and it will create board with 'size' rows & 'size' columns:");
        String boardSizeStr = reader.nextLine();
        while(!isValidBoardSize(boardSizeStr)) {
            boardSizeStr = reader.nextLine();
        }
        int boardSize = Integer.parseInt(boardSizeStr);
        
        //Initialize the game.
        g = new Game(boardSize, playerCount);
        
        
        // Create and add computer as a player
        Computer comp = new Computer();
        players.add(comp);
        markers.add(comp.getMarker());
        comp.introduceSelf();
        
        // Get player info
        for(int i=1; i < g.getPlayerCount(); i++) {
            players.add(getPlayerInfo(i));
        }
             
        System.out.println("Let's begin the game");
        while(true) {
            for(int i=0; i < players.size(); i++) {
                g.printBoard();
                Player currentPlayer = players.get(i);
                while(true) {
                    System.out.println(currentPlayer.getName()+"'s turn:");
                    int move;
                    if(currentPlayer instanceof Computer) {
                         move = comp.decideMove(g);
                    } else {
                        String userInput = reader.nextLine();
                         while(!isValidMoveInput(userInput)) {
                            userInput = reader.nextLine();
                         }
                         move = Integer.parseInt(userInput);
                    }
                    if(g.isValidMove(move)) { // Check if move is valid before playing
                        // declare winner if current move was a winning move else continue
                        if(g.makeMove(currentPlayer, move)) {
                            g.printBoard();
                            System.out.println(currentPlayer.getName() +" won. Thanks for playing");
                            System.exit(0);
                        } else if(g.isStaleMate()) {
                            System.out.println("Looks like its a draw. Thanks for playing");
                            System.exit(0);
                        }
                        break;
                    } else {
                        System.err.println("Invalid move. Please play correctly");
                    }
                }
            }
        }
    }

    private static Player getPlayerInfo(int i) {
        System.out.println("Hello player "+i +", how can I address you?");
        Scanner reader = new Scanner(System.in); 
        String name = reader.nextLine(); 
        while(name.trim().isEmpty()) {
            System.out.println("Invalid name. Please provide a valid name:");
            name = reader.nextLine(); 
        }
        
        String markersStr = "";
        for(Character marker : markers) {
            markersStr += marker;
            markersStr += ",";
        }
        System.out.println("Please choose a single letter marker apart from " + markersStr);
        char marker = reader.nextLine().charAt(0); 
        while(markers.contains(marker)) {
            System.out.println("That marker is already used. Please select any other marker to continue:");
            marker = reader.nextLine().charAt(0); 
        }
        markers.add(marker);
        System.out.println("Nice meeting you "+name+". We will be using "+(char)marker+" as your marker for the game");
        
        return new Player(i, name, marker);
    }
    
    private static boolean isValidMoveInput(String move){
       if(isValidInteger(move)){
            int val = Integer.parseInt(move);
            int boardSize = g.getBoardSize();
            if(val < 0 || val > boardSize*boardSize - 1) {
                System.err.println("Value of move must range from 0 to "+ (boardSize*boardSize-1));
                return false;
            }
        } else {
           return false;
       }
        return true;
    }
    
    private static boolean isValidInteger(String input) {
        try {
            Integer.parseInt(input);
        } catch(NumberFormatException ne) {
            System.err.println("Invalid input. Please enter a valid number:");
            return false;
        }
        return true;
    }
    
    private static boolean isValidBoardSize(String input) {
        if(isValidInteger(input)){
            int val = Integer.parseInt(input);
            if(val%2 == 0) {
                System.err.println("Invalid board size. Please enter a board size that is an odd number:");
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
}
