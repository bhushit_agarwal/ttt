package bagarwal.spreetail.ttt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author bhush
 */
public class Computer extends Player{

    public Computer() {
        super(0, "Computer", 'X');
    }
    
    public void introduceSelf() {
        System.out.println("Hello, my name is " + this.getName().toString() + " and I like to go by " + (char)this.getMarker());
    }
    
    public int decideMove(Game g) {
        List<Integer> availableMoves = g.getAvailableMoves();
        
        // Check winning move for computer 
        int compWinningMove = checkWinningMove(g, this.getId());
        if(compWinningMove != -1) {
            return checkWinningMove(g, this.getId());
        }
        
        // Block if player is winning
        int playerWinningMove = -1;
        for(int i=1; i < g.getPlayerCount(); i++) {
            playerWinningMove = checkWinningMove(g, i);
            if(playerWinningMove != -1) {
                return playerWinningMove;
            }
        }
        
        int rand = new Random().nextInt(availableMoves.size());
        return availableMoves.get(rand);
    }
    
    private int checkWinningMove(Game g, int playerId){
        List<Integer> availableMoves = g.getAvailableMoves();
        int boardSize = g.getBoardSize();
        // Check for winning move in rows and cols
        Move move = g.getMoves().get(playerId);
        for(int i=0;i<boardSize; i++) {
            boolean rows = move.getRows().getOrDefault(i, new ArrayList<>()).size() == boardSize - 1;
            boolean cols = move.getColumns().getOrDefault(i, new ArrayList<>()).size() == boardSize - 1;
            
            if(rows) {
                for(int j=0; j<boardSize; j++) {
                    if(availableMoves.contains(i*boardSize+j))
                        return i*boardSize+j;
                }
            }
            
            if(cols) {
                for(int j=0; j<boardSize; j++) {
                    if(availableMoves.contains(j*boardSize+i))
                        return j*boardSize+i;
                }
            }
        }
        
        // Check winning move in diagonals
        if(move.getDiagonals().getOrDefault(0,new ArrayList<>()).size() == boardSize - 1) {
            for(int i=0; i<boardSize; i++) {
                if(availableMoves.contains(i*boardSize+i))
                    return i*boardSize+i;
            }
        }
        if(move.getDiagonals().getOrDefault(1,new ArrayList<>()).size() == boardSize -1) {
            for(int i=0; i<boardSize; i++) {
                int j = boardSize - i - 1;
                if(availableMoves.contains(i*boardSize+j))
                    return i*boardSize+j;
            }
        }
        return -1;
    }
}
