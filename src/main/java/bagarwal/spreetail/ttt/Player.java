package bagarwal.spreetail.ttt;

/**
 *
 * @author bhush
 */
public class Player {
    private final int id;
    private final String name;
    private final char marker;

    public Player(int id, String name, char marker) {
        this.id = id;
        this.name = name;
        this.marker = marker;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public char getMarker() {
        return marker;
    }  
    
}
