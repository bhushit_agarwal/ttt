package bagarwal.spreetail.ttt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author bhush
 */
class Move {
    
    private final Map<Integer, List<Integer>> rows;
    private final Map<Integer, List<Integer>> columns;
    private final Map<Integer, List<Integer>> diagonals;
    
    public Move() {
        rows = new HashMap<>();
        columns = new HashMap<>();
        diagonals = new HashMap<>();
    }

    public Map<Integer, List<Integer>> getRows() {
        return rows;
    }

    public Map<Integer, List<Integer>> getColumns() {
        return columns;
    }

    public Map<Integer, List<Integer>> getDiagonals() {
        return diagonals;
    }
       
}
